import math

def rotation(s, ix, iy, rot):
	length = len(s)
	if(length == 4):
		return iy
	else:
		r = math.sqrt(length)
		indexing = {
			0 : iy * r + ix,
			1 : (r*3) + iy - (ix*r), 
			2 : (r*r-1) - (iy*r) - ix,
			3 : (r-1) - iy + (ix*r)
		}
		return indexing.get(rot)
		

